import networkit as nk
from src.tools.graph import Objective
from src.tools.supports import *
import json

from src.algorithms.greedy import *
from src.algorithms.fast import *


logger = init_log(logname=__name__, filename="submodular.log", console = True)

def main(configs):

    nk.setNumberOfThreads(configs["resource"]["number_of_threads"])
    # seed_everything(seed = 42)
    # Create undirected graph
    graphReader = nk.graphio.EdgeListReader(separator = ",", firstNode = 0, directed = True, continuous = True)
    G = graphReader.read(configs["graph"]["data_path"])
    G.removeSelfLoops()
    G.indexEdges()
    # print([node for node in G.iterNodes()])
    # print([edge for edge in G.iterEdgesWeights()])

    obj = Objective(G, 100)
    N = obj.shape
    B = 1
    budget_vector = B * np.ones(N)
    sol = greedy(obj, 10, budget_vector, epsilon=0.05)
    print(sol)

if __name__ == "__main__":
    configs = json.load(open("configs/configs.json"))
    main(configs)

