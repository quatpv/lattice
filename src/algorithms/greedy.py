import numpy as np
from src.tools.supports import *

logger = init_log(logname=__name__, filename="greedy.log", console = True)

def unit(node, N):
    vector = np.zeros(N)
    vector[node] = 1
    return vector


def random_sample(sol, X, N, B, k):
    A = []
    sum_sol = np.sum(sol)
    cur_sol = np.zeros(N)

    while sum_sol + len(A) < k:
        sample = []
        for e in X:
            # B >= cur_sol + unit(e, N)
            if np.greater_equal(B, sol + cur_sol + unit(e, N)).all():
                sample.append(e)
        if sample:
            a = random.sample(sample, 1)[0]
            A.append(a)
            cur_sol += unit(a, N)
            X = sample
        else:
            break
    return A


def lattice_threshold(obj, N, sol, theta, epsilon, B, k): 
    # B >= sol + unit(e, N)
    X = [e for e in obj.ground_set if np.greater_equal(B, sol + unit(e, N)).all()]
    y = np.zeros(N)
    while len(X):
        A = random_sample(sol + y, X, N, B, k)
        m = len(A)
        X_new = [X]
        for j in range(len(A)):
            Xj = []
            cur_sol = sol.copy()
            for a in A[:j+1]:
                cur_sol += unit(a, N)
            for e in X:
                if np.greater_equal(B, cur_sol + unit(e, N)).all() and obj.delta(cur_sol, e) >= theta * (j + 1):
                    Xj.append(e)
            X_new.append(Xj)
        X_new.append([])
        i_star = 0
        for i in range(m):
            if len(X_new[i]) >= (1 - epsilon) * len(X):
                i_star = i

        for a in A[:i_star + 1]:
            y += unit(a, N)

        if sum(sol + y) == k:
            break
        
        X = X_new[i_star+1]
    
    return y

            
def greedy(obj, k, B, epsilon=0.01):
    assert 0 < epsilon < 1.0
    N = obj.shape
    sol = np.zeros(N)
    theta = max([obj.delta(sol, node) for node in obj.ground_set])
    i = 1
    while np.sum(sol) < k and i < np.log2(k / epsilon**2):
        y = lattice_threshold(obj, N, sol, theta, epsilon, B, k)
        sol += y
        theta *= (1 - epsilon)
        i += 1
    return sol


def fast(obj, k, B, epsilon):
    M = max([obj.delta([], node) for node in obj.ground_set])
    res = []
    for i in range(1, 1/epsilon * np.log2(k)):
        x = np.zeros(obj.shape)
        opt = (1 - epsilon) * k * M
        
        for j in range(0, 1/epsilon * np.log2(1/epsilon)):
            l = 0
            while sum(x) < k and l < (1 - epsilon)**(j-1)/epsilon:
                theta = (1 - epsilon)**(j + 1) * (opt - obj.value(x)) / k
                x += lattice_threshold(obj, x, theta, epsilon, budget = B)
                l += 1
        res.append(x)
    x = np.argmax([obj.value(x) for x in res])
    return x
