from collections import deque
import numpy as np


class Objective:
    def __init__(self, G, number_sample, base=0.01, prog=0.1):
        """Influence maximization
        Args:
            G [nk.Graph()]  : A networkit Graph
            nodes [int]     : List node of G
        """

        self.ground_set = [node for node in G.iterNodes()]
        self.shape = len(self.ground_set)
        self.edges = [edge for edge in G.iterEdgesWeights()]
        self.G = G
        self.base = base
        self.number_sample = number_sample
        self.prog = prog

    def formular(self, base, count, prog):
        return base + count * prog

    def bfs(self, graph, start_set, sol, more_check=False, active_nodes=[]):
        visited = start_set
        queue = deque(start_set)
        while queue:
            node = queue.popleft()
            for neighbor, weight in self.G.iterNeighborsWeights(node):
                if neighbor not in visited:
                    rand   = graph["edge_weights"][self.G.edgeId(node, neighbor)]
                    weight = self.formular(weight, sol[neighbor], self.prog)
                    if more_check:
                        more_cor = not (neighbor in active_nodes)
                    else:
                        more_cor = True
                    if rand <= weight and more_cor:
                        queue.append(neighbor)
                        visited.append(neighbor)
        
        return visited

    def active_nodes(self, graph, S):
        active = []
        for node in self.ground_set:
            weight = self.formular(self.base, S[node], self.prog)
            rand   = graph["node_weights"][self.ground_set.index(node)]
            if rand <= weight:
                active.append(node)
        if len(active) == 0:
            return active

        active = self.bfs(graph, active, S)
        return active

    def samples_graph(self, S):
        """Return list sample graph and active nodes of the graph reponsive"""
        for _ in range(self.number_sample):
            graph = {
                "node_weights": np.random.uniform(0.0, 1.0, len(self.ground_set)),
                "edge_weights": np.random.uniform(0.0, 1.0, self.G.numberOfEdges())
            }
            active_nodes = self.active_nodes(graph, S)
            yield {"graph": graph, "active_nodes": active_nodes}

    def delta(self, sol, node):
        """Return F(S + node) - F(S)"""

        delta = 0

        samples = self.samples_graph(sol)
        for sample in samples:
            graph = sample["graph"]
            active_nodes = sample["active_nodes"]

            # true if the element is already active in this sample
            already_active = node in active_nodes
            # true if the element would be activated by external forces if we increment it
            would_externally_activate = graph["node_weights"][node] <= self.formular(self.base, sol[node] + 1, self.prog)
            # true if the element would be activated by a neighboring node if we increment it
            would_edge_activate = False

            for neighbor, weight in self.G.iterInNeighborsWeights(node):
                rand = graph["edge_weights"][self.G.edgeId(neighbor, node)]
                weight = self.formular(weight, sol[node] + 1, self.prog)
                if neighbor in active_nodes and rand <= weight:
                    would_edge_activate = True
                    break
            if already_active or (not would_externally_activate and not would_edge_activate):
                delta += 0.0
            else:
                delta += len(self.bfs(graph, [node], sol, more_check=True, active_nodes=active_nodes))

        return delta / self.number_sample
