import numpy as np
import logging
import random

def init_log(logname=__name__, filename = "submodular.log", level=logging.DEBUG, console=True):
    # set up logging to file - see previous section for more details
    logging.basicConfig(level=level,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%Y-%m-%d %H:%M',
                        filename=filename,
                        filemode='w')
    # Now, define a couple of other loggers which might represent areas in your
    # application:
    logger = logging.getLogger(logname)
    if console:
        # define a Handler which writes INFO messages or higher to the sys.stderr
        console = logging.StreamHandler()
        console.setLevel(logging.INFO)
        # set a format which is simpler for console use
        formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
        # tell the handler to use this format
        console.setFormatter(formatter)
        # add the handler to the the current logger
        logger.addHandler(console)
    return logger


def check_inputs(inf_func, k):
    """Function to run basic tests on the inputs of one of our optimization functions:

    Args:
        inf_func (object)   : contains the methods 'value()' that we want to optimize and its marginal value function 'margin_value()' 
        k (int)             : the cardinality constraint
    """

    # inf_func class contains the ground set and also value, margin_value methods
    assert(hasattr(inf_func, 'ground_set'))
    assert(hasattr(inf_func, 'value'))
    
    # k is greater than 0
    assert(k > 0)

    # k is smaller than the number of elements in the ground set
    assert(k < len(inf_func.ground_set) )

    # the ground set contains all integers from 0 to the max integer in the set
    assert(len(inf_func.ground_set) == np.max(inf_func.ground_set) + 1)
    # assert(np.array_equal(inf_func.ground_set, list(range(np.max(inf_func.ground_set)+1))))


def seed_everything(seed=7497):
    random.seed(seed)
    np.random.seed(seed)
